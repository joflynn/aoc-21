package util

type SyntaxTree struct {
	token    string
	matched  bool
	children []SyntaxTree
}

func NewSyntaxTreeNode(token string) *SyntaxTree {
	return &SyntaxTree{token, false, make([]SyntaxTree, 0)}
}

func (t *SyntaxTree) Token() string {
	return t.token
}
func (t *SyntaxTree) AddChild(token string) *SyntaxTree {
	child := NewSyntaxTreeNode(token)
	t.children = append(t.children, *child)
	return child
}
func (t *SyntaxTree) Close() *SyntaxTree {
	t.matched = true
	return t
}
func (t *SyntaxTree) Open() bool {
	return !t.matched
}
