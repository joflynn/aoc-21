package util

type Queue struct {
	queue []interface{}
}

func NewQueue() *Queue {
	return &Queue{queue: make([]interface{}, 0)}
}

func (q *Queue) Push(node interface{}) {
	q.queue = append(q.queue, node)
}
func (q *Queue) Empty() bool {
	return len(q.queue) == 0
}

func (q *Queue) Front() interface{} {
	if len(q.queue) == 0 {
		return nil
	}

	node := q.queue[0]
	q.queue[0] = nil
	q.queue = q.queue[1:]
	return node
}
