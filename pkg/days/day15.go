package days

import (
	"bufio"
	"container/heap"
	"fmt"
	"io"
	"math"
)

type Pt struct {
	x, y int
}

func Day15(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)

	heightMap := ParseHeightmap(scanner)
	if part == PART_TWO {

		//heightMap.Show(10)
		heightMap = ExpandHeightMap(heightMap)
		//heightMap.Show(10)
	}

	start := Pt{0, 0}
	dest := Pt{int(heightMap.length - 1), int(heightMap.width - 1)}

	total := Djikstra(heightMap, start, dest)
	return fmt.Sprint(total), nil
}

func Djikstra(m HeightMap, s, e Pt) int {
	dist := make(map[Pt]int)
	visited := make(map[Pt]bool)
	lookup := make(map[Pt]*Item)
	dist[s] = 0
	visited[s] = true
	queue := make(PriorityQueue, 0)
	for j := 0; j < m.width; j += 1 {
		for i := 0; i < m.length; i += 1 {
			point := Pt{int(i), int(j)}
			item := Item{point, math.MaxInt16, 0}
			lookup[point] = &item
			if point == s {
				item.priority = 0
			}
			queue.Push(&item)
		}
	}
	heap.Init(&queue)

	for queue.Len() > 0 {
		current := queue.Pop().(*Item)
		//fmt.Println(current)
		if current.value == e {
			return current.priority
		}
		neighbors := m.NeighborPts(current.value)
		for _, neighbor := range neighbors {
			next := Pt{int(neighbor.x), int(neighbor.y)}
			if visited[next] {
				continue
			}
			newDist := current.priority+int(neighbor.height)
			//fmt.Printf("Checking neighbor of %d, %d, distance %d\n", current, next, newDist)
			queue.Update(lookup[next], next, newDist)
			dist[next] = newDist
			visited[next] = true
			//fmt.Println("-")
		}
		//visited[current.value] = previous
		// if ( len(dist) > 10) {
		// 	break
		// }
	}
	fmt.Println(dist)
	//queue.Show()
	return 0
}

type Item struct {
	value    Pt  // The value of the item; arbitrary.
	priority int // The priority of the item in the queue.
	// The index is needed by update and is maintained by the heap.Interface methods.
	index int // The index of the item in the heap.
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }

//min-pq
func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].priority < pq[j].priority
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*Item)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[0]
	old[0] = nil    // avoid memory leak
	item.index = -1 // for safety
	*pq = old[1:n]
	return item
}

// update modifies the priority and value of an Item in the queue.
func (pq *PriorityQueue) Update(item *Item, value Pt, priority int) {
	//fmt.Printf("Update %d with new priority %d\n",item, priority)
	item.value = value
	item.priority = priority
	//heap.Fix(pq, item.index)
	heap.Init(pq)
}

func (pq *PriorityQueue) Show() {
	for _, x := range *pq {
		fmt.Print(x)
	}
	fmt.Println()
}

func (m HeightMap) NeighborPts(p Pt) []HeightPoint {
	ret := make([]HeightPoint, 0, 4)
	//fmt.Printf("%d %d %d %d\n", y, x, m.length, m.width)
	if p.y > 0 {
		//north
		ret = append(ret, HeightPoint{m.grid[p.y-1][p.x], p.y - 1, p.x})
	}
	if p.x < m.width-1 {
		//east
		ret = append(ret, HeightPoint{m.grid[p.y][p.x+1], p.y, p.x + 1})
	}
	if p.y < m.length-1 {
		//south
		ret = append(ret, HeightPoint{m.grid[p.y+1][p.x], p.y + 1, p.x})
	}
	if p.x > 0 {
		//west
		ret = append(ret, HeightPoint{m.grid[p.y][p.x-1], p.y, p.x - 1})
	}
	return ret
}

func (m HeightMap) Show(spacer int) {
	for j, row := range m.grid {
		if j % spacer == 0 {
			fmt.Println()
		}
		for i, cell := range row {
			if i % spacer == 0 {
				fmt.Print(" ")
			}
			fmt.Print(cell)
		}
		fmt.Println()
	}
	fmt.Println()
}

func ExpandHeightMap(m HeightMap) HeightMap {
	grid := make([][]int64, 5*m.length)
	for j := 0; j < 5*m.length; j+= 1 {
		row := make([]int64, 5*m.width)
		for i := 0; i < 5*m.width; i+=1 {
			orig := m.grid[j % m.length][i % m.width] 
			offset := Offset(m, i, j) 
			next := (orig + int64(offset))
			if next == 19 {
				row[i] = 1
			} else if (next > 9) {
				row[i] = (next % 10) + (next / 10)
			} else {
				row[i] = next
			}
		}
		grid[j] = row
	}
	ret := HeightMap{grid, 5*m.length, 5*m.width }
	return ret
}
func Offset( m HeightMap, x, y int) int {
	yOffset := y / m.length
	xOffset := x / m.width
	return yOffset + xOffset
}