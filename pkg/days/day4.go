package days

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"

	"gitlab.com/joflynn/aoc-21/pkg/bingo"
)

func Day4(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)

	scanner.Scan()
	draws := ParseDraws(scanner.Text())
	//fmt.Println(draws)

	var boards []bingo.IBoard
	for scanner.Scan() {
		board, err := bingo.NewBoard(scanner)
		if err != nil {
			return "", err
		}
		boards = append(boards, board)
	}

	var lastDraw int64
	var lastBoard int
	losers := len(boards)
	for _, draw := range draws {
		//fmt.Printf("%d Draw: %d\n", i, draw)
		for j, b := range boards {
			if !b.Won() && b.Mark(draw) {
				//fmt.Printf("Match on board %d\n", j)
				if b.Won() {
					//fmt.Printf("Bingo %d\n", j)
					if part == PART_ONE {
						return fmt.Sprint(draw * b.Value()), nil
					} else {
						losers -= 1
					}
					lastBoard = j
					lastDraw = draw
				}
			}
		}
		if losers == 0 {
			//fmt.Printf("last draw %d, lastValue %d", lastDraw, boards[lastBoard].Value())
			return fmt.Sprint(lastDraw * boards[lastBoard].Value()), nil
		}
	}

	return "", nil
}

func ParseDraws(data string) (ret []int64) {
	for _, val := range strings.Split(data, ",") {
		v, err := strconv.ParseInt(val, 10, 64)
		if err == nil {
			ret = append(ret, v)
		}
	}
	return
}
