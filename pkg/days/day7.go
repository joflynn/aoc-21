package days

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

func Day7(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)

	scanner.Scan()
	positions := ParseCommaSeparatedInts(scanner.Text())

	var xMin, xMax int64
	for _, x := range positions {
		if x < xMin {
			xMin = x
		}
		if x > xMax {
			xMax = x
		}
	}

	fuelMin := int64(9999999999)
	cost := LinearCost
	if part == PART_TWO {
		cost = TriangularCost
	}
	costs := make([]int64, len(positions))
	//fmt.Printf("min %d, max %d\n", xMin, xMax)
	for i := xMin; i < xMax; i += 1 {
		for j, sub := range positions {
			costs[j] = cost(sub, i)
		}
		total := sum(costs)
		if total < fuelMin {
			//fmt.Printf("Min found at %d, %d\n", i, total)
			fuelMin = total
		}
	}

	return fmt.Sprint(fuelMin), nil
}

func ParseCommaSeparatedInts(data string) []int64 {
	split := strings.Split(data, ",")
	list := make([]int64, 0)
	for _, v := range split {
		i, err := strconv.ParseInt(v, 10, 64)
		if err == nil {
			list = append(list, i)
		}
	}
	return list
}
func LinearCost(from, to int64) int64 {
	if from > to {
		return from - to
	}
	return to - from
}
func TriangularCost(from, to int64) int64 {
	diff := LinearCost(from, to)
	return (diff * (diff + 1)) / 2
}
