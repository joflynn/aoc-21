package days

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"strconv"
)

func Day3(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)

	var epsilon, gamma, o2, co2 int64

	var length, width int

	values := make([]int64, 0)
	i := 0
	for scanner.Scan() {
		line := scanner.Text()
		value, err := ParseDiagnostic(line)
		if err != nil {
			return "", err
		}
		values = append(values, value)
		w := int(math.Ceil(math.Log2(float64(value))))
		if w > width {
			width = w
		}
		i += 1
	}
	length = len(values)

	for j := 0; j < width; j++ {
		count := 0
		var mask int64 = 1 << j
		for _, cur := range values {
			if (cur & mask) == mask {
				count += 1
			}
		}
		if count > (length / 2) {
			epsilon = epsilon | mask
		} else {
			gamma = gamma | mask
		}
	}

	if part == PART_ONE {
		//fmt.Printf("Epsilon %d, Gamma %d\n", epsilon, gamma)
		return fmt.Sprint(epsilon * gamma), nil
	} else {

		o2 = SearchByPrefix(values, width, true)
		co2 = SearchByPrefix(values, width, false)

		//fmt.Printf("O2 %d, CO2 %d\n", o2, co2)
		return fmt.Sprint(o2 * co2), nil

	}

}
func SearchByPrefix(values []int64, width int, useMode bool) int64 {

	//var value int64
	set := values[:]

	for k := width - 1; k >= 0; k-- {
		if len(set) > 1 {
			mode := ModeAtPos(set, k, true)
			mask := int64(1 << k)
			//keep values if their bit matches the mode a
			set = Filter(set, func(v int64) bool {
				hasBit := (v & mask) == mask
				if useMode {
					return hasBit == mode
				}
				return hasBit != mode
			})
		}
	}
	return set[0]
}

func Filter(values []int64, test func(int64) bool) (ret []int64) {
	for _, v := range values {
		if test(v) {
			ret = append(ret, v)
		}
	}
	return
}

func ModeAtPos(values []int64, pos int, tiebreaker bool) bool {
	mask := int64(1 << pos)
	count := 0
	for _, v := range values {
		if (v & mask) == mask {
			count += 1
		}
	}
	if (count * 2) == len(values) {
		return tiebreaker
	}
	return (count * 2) >= len(values)
}

func ParseDiagnostic(data string) (int64, error) {
	i, err := strconv.ParseInt(data, 2, 64)
	if err != nil {
		return 0, err
	}
	return i, nil
}
