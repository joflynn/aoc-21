package days

import (
	"strings"
	"testing"
)

const day3Input = `00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010`

const expectedDay3Part1 = "198"

func TestDay3Part1(t *testing.T) {
	reader := strings.NewReader(day3Input)
	value, err := Day3(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay3Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay3Part1, value)
	}
}

const expectedDay3Part2 = "230"

func TestDay3Part2(t *testing.T) {
	reader := strings.NewReader(day3Input)
	value, err := Day3(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay3Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay3Part2, value)
	}
}
