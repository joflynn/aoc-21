package days

import (
	"bufio"
	"fmt"
	"io"
	"strings"
	"time"
)

func Day12(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)

	cave, err := ParseGraph(scanner)
	if err != nil {
		return "", err
	}
	//fmt.Println(cave.neighbors)
	trailCount := 0
	trails := make(chan string, 32)
	go SearchCave(make([]string, 0), "start", cave, trails, part == PART_TWO)

	for {
		select {
		case _ = <-trails:
			trailCount += 1
			//fmt.Println(trail)
		case <-time.After(time.Second):
			return fmt.Sprint(trailCount), nil
		}
	}
}

type CaveGraph struct {
	nodes     []string
	neighbors map[string][]string
}

func ParseGraph(scanner *bufio.Scanner) (CaveGraph, error) {
	neighbors := make(map[string][]string)
	nodes := make([]string, 0)
	for scanner.Scan() {
		nodes := strings.Split(scanner.Text(), "-")
		if len(nodes) != 2 {
			return CaveGraph{}, fmt.Errorf("Unable to parse edge")
		}
		if neighbors[nodes[0]] == nil {
			nodes = append(nodes, nodes[0])
		}
		if neighbors[nodes[1]] == nil {
			nodes = append(nodes, nodes[1])
		}
		neighbors[nodes[0]] = append(neighbors[nodes[0]], nodes[1])
		neighbors[nodes[1]] = append(neighbors[nodes[1]], nodes[0])
	}
	return CaveGraph{nodes, neighbors}, nil
}

func IsLargeCave(node string) bool {
	return node == strings.ToUpper(node)
}
func IsNotInTrail(trail []string, test string) bool {
	for _, cur := range trail {
		if cur == test {
			return false
		}
	}
	return true
}

func SearchCave(trail []string, cur string, cave CaveGraph, out chan<- string, canRevisitASmallCave bool) {
	//fmt.Printf("SearchCave %s %s\n", trail, cur)
	nextTrail := append(trail, cur)
	if cur == "end" {
		out <- strings.Join(nextTrail, ",")
		return
	}
	for _, next := range cave.neighbors[cur] {
		if next == "start" {
			continue
		}
		notInTrail := IsNotInTrail(trail, next)
		if IsLargeCave(next) || notInTrail {
			//recurse
			SearchCave(nextTrail, next, cave, out, canRevisitASmallCave)
		} else if !notInTrail && canRevisitASmallCave {
			SearchCave(nextTrail, next, cave, out, false)
		}
	}
}
