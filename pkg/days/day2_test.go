package days

import (
	"strings"
	"testing"
)

const day2Input = `forward 5
down 5
forward 8
up 3
down 8
forward 2
`

const expectedDay2Part1 = "150"

func TestDay2Part1(t *testing.T) {
	reader := strings.NewReader(day2Input)
	value, err := Day2(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error")
	}
	if value != expectedDay2Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay2Part1, value)
	}
}

const expectedDay2Part2 = "900"

func TestDay2Part2(t *testing.T) {
	reader := strings.NewReader(day2Input)
	value, err := Day2(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error")
	}
	if value != expectedDay2Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay2Part2, value)
	}
}
