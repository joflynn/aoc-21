package days

import (
	"strings"
	"testing"
)

const Day13Input = `6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5`

const expectedDay13Part1 = "17"

func TestDay13Part1(t *testing.T) {
	reader := strings.NewReader(Day13Input)
	value, err := Day13(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay13Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay13Part1, value)
	}
}

const expectedDay13Part2 = `
#####
#...#
#...#
#...#
#####
`

func TestDay13Part2(t *testing.T) {
	reader := strings.NewReader(Day13Input)
	value, err := Day13(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay13Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay13Part2, value)
	}
}
