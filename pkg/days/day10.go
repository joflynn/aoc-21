package days

import (
	"bufio"
	"fmt"
	"io"
	"sort"
	"strings"

	"gitlab.com/joflynn/aoc-21/pkg/util"
)

func Day10(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)

	syntaxPoints := 0
	incompletePoints := make([]int, 0)
	for scanner.Scan() {
		line := scanner.Text()
		//fmt.Println(line)
		tokens := util.NewQueue()
		for _, char := range strings.Split(line, "") {
			tokens.Push(char)
		}
		_, err := ParseSyntaxTree(tokens, util.NewSyntaxTreeNode("-"))
		if err != nil {
			if strings.Contains(err.Error(), "Expected") {
				syntaxPoints += scoreSyntaxError(err)
				//fmt.Printf("%s, %d\n", err, syntaxPoints)
			} else {
				//fmt.Printf("Complete by adding %s\n", err)
				incompletePoints = append(incompletePoints, scoreIncompleteLine(err))
			}
		}
	}
	if part == PART_ONE {
		return fmt.Sprint(syntaxPoints), nil
	} else {
		sort.Ints(incompletePoints)

		return fmt.Sprint(median(incompletePoints)), nil
	}
}

func ParseSyntaxTree(tokens *util.Queue, context *util.SyntaxTree) (*util.SyntaxTree, error) {

	var err error
	for !tokens.Empty() && err == nil {
		token := tokens.Front().(string)
		if isClosingToken(token) {
			//check corrupt
			if !tokensMatch(context.Token(), token) {
				//fmt.Printf("Expected %s, but found %s instead\n", matchingToken(context.Token()), token)
				return context, fmt.Errorf("Expected %s, but found %s instead", matchingToken(context.Token()), token)
			}
			return context.Close(), nil
		} else {
			//recurse
			child := context.AddChild(token)
			_, err = ParseSyntaxTree(tokens, child)
		}
	}
	if context.Open() && context.Token() != "-" {
		previous := ""
		if err != nil {
			if strings.Contains(err.Error(), "Expected") {
				return context, err
			}
			previous = err.Error()
		}
		err = fmt.Errorf("%s%s", previous, matchingToken(context.Token()))
	}
	return context, err
}

func isClosingToken(token string) bool {
	return token == ")" || token == "]" || token == "}" || token == ">"
}
func tokensMatch(a, b string) bool {
	return (a == "(" && b == ")") ||
		(a == "[" && b == "]") ||
		(a == "{" && b == "}") ||
		(a == "<" && b == ">")
}
func matchingToken(token string) string {
	switch token {
	case "(":
		return ")"
	case "[":
		return "]"
	case "{":
		return "}"
	case "<":
		return ">"
	}
	return "-"
}
func scoreSyntaxError(e error) int {
	switch {
	case strings.Contains(e.Error(), "found )"):
		return 3
	case strings.Contains(e.Error(), "found ]"):
		return 57
	case strings.Contains(e.Error(), "found }"):
		return 1197
	case strings.Contains(e.Error(), "found >"):
		return 25137
	}
	return 0
}
func scoreIncompleteLine(e error) int {
	score := 0
	for _, char := range strings.Split(e.Error(), "") {
		add := 0
		switch {
		case strings.Contains(char, ")"):
			add = 1
		case strings.Contains(char, "]"):
			add = 2
		case strings.Contains(char, "}"):
			add = 3
		case strings.Contains(char, ">"):
			add = 4
		}

		score *= 5
		score += add
	}
	return score
}
func median(slice []int) int {
	return slice[len(slice)/2]
}
