package days

import (
	"bufio"
	"fmt"
	"io"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/joflynn/aoc-21/pkg/util"
)

type HeightMap struct {
	grid          [][]int64
	length, width int
}
type HeightPoint struct {
	height int64
	y, x   int
}

func Day9(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)

	heightMap := ParseHeightmap(scanner)
	localMinima := make([]HeightPoint, 0)
	for i := 0; i < heightMap.length; i += 1 {
		for j := 0; j < heightMap.width; j += 1 {
			neighbors := heightMap.MapNeighbors(i, j)
			current := heightMap.grid[i][j]

			isMin := true
			isFlat := true
			for _, n := range neighbors {
				if current > n {
					isMin = false
				}
				if current != n {
					isFlat = false
				}
			}
			if isMin && !isFlat {
				//fmt.Printf("%d %d %d %d\n", current, i, j, neighbors)
				localMinima = append(localMinima, HeightPoint{current, i, j})
			}
		}
	}
	if part == PART_ONE {
		sum := int64(0)
		for _, p := range localMinima {
			sum += p.height
		}
		total := sum + int64(len(localMinima))
		return fmt.Sprint(total), nil
	}

	basinSizes := make([]int, 0)
	for _, minimum := range localMinima {
		basinSize := int(measureBasin(heightMap, minimum))
		//fmt.Printf("Basin size %d\n", basinSize)
		basinSizes = append(basinSizes, basinSize)
	}
	sort.Ints(basinSizes)
	return fmt.Sprint(product(basinSizes[len(basinSizes)-3:])), nil
}

func ParseHeightmap(scanner *bufio.Scanner) HeightMap {
	ret := make([][]int64, 0)
	width := 0
	for scanner.Scan() {
		line := strings.Split(scanner.Text(), "")
		row := make([]int64, 0)
		for _, char := range line {
			digit, err := strconv.ParseInt(char, 10, 64)
			if err == nil {
				row = append(row, int64(digit))
			}
		}
		width = len(row)
		ret = append(ret, row)
	}

	return HeightMap{ret, len(ret), width}
}

func (m HeightMap) MapNeighbors(y, x int) []int64 {
	ret := make([]int64, 0, 4)
	//fmt.Printf("%d %d %d %d\n", y, x, m.length, m.width)
	if y > 0 {
		//north
		ret = append(ret, m.grid[y-1][x])
	}
	if x < m.width-1 {
		//east
		ret = append(ret, m.grid[y][x+1])
	}
	if y < m.length-1 {
		//south
		ret = append(ret, m.grid[y+1][x])
	}
	if x > 0 {
		//west
		ret = append(ret, m.grid[y][x-1])
	}
	return ret
}
func (m HeightMap) NeighborPoints(p HeightPoint) []HeightPoint {
	ret := make([]HeightPoint, 0, 4)
	//fmt.Printf("%d %d %d %d\n", y, x, m.length, m.width)
	if p.y > 0 {
		//north
		ret = append(ret, HeightPoint{m.grid[p.y-1][p.x], p.y - 1, p.x})
	}
	if p.x < m.width-1 {
		//east
		ret = append(ret, HeightPoint{m.grid[p.y][p.x+1], p.y, p.x + 1})
	}
	if p.y < m.length-1 {
		//south
		ret = append(ret, HeightPoint{m.grid[p.y+1][p.x], p.y + 1, p.x})
	}
	if p.x > 0 {
		//west
		ret = append(ret, HeightPoint{m.grid[p.y][p.x-1], p.y, p.x - 1})
	}
	return ret

}

func measureBasin(m HeightMap, start HeightPoint) int64 {
	//fmt.Printf("measure basin, %d\n", start)
	//var spillDepth int64
	visited := make(map[HeightPoint]bool)
	visited[start] = true
	queue := util.NewQueue()
	queue.Push(start)
	var spillHeight int64
	for !queue.Empty() {
		spillHeight, _ = measureBasinHelper(m, queue, visited)
	}
	//fmt.Printf("measureBasinHelper %d\n", spillHeight)
	//fmt.Println(visited)
	size := int64(0)
	for cell := range visited {
		if cell.height < spillHeight {
			size += 1
		}
	}

	return size
}

func measureBasinHelper(m HeightMap, queue *util.Queue, visited map[HeightPoint]bool) (int64, bool) {
	current, ok := queue.Front().(HeightPoint)
	if !ok {
		return 0, false
	}
	//fmt.Println(current)
	if current.height == 9 {
		return current.height, false
	}
	spillHeight := int64(9)
	for _, neighbor := range m.NeighborPoints(current) {
		//fmt.Println(neighbor)
		if visited[neighbor] != true {

			if current.height > neighbor.height {
				//stop
				//fmt.Printf("Spill at %d %d\n", current, neighbor)
				return current.height, true
			}

			visited[neighbor] = true
			queue.Push(neighbor)
		}
	}
	return spillHeight, false
}

func product(buf []int) int {
	total := int(1)
	for _, cur := range buf {
		total *= cur
	}
	return total
}
