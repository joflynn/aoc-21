package days

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

func Day5(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)
	domain := make([]int64, 2, 2)
	rng := make([]int64, 2, 2)

	lines := make([]Line, 0, 0)
	for scanner.Scan() {
		line, err := ParseLine(scanner.Text())
		if err != nil {
			return "", err
		}
		if part == PART_TWO || (line.horizontal || line.vertical) {
			lines = append(lines, line)
		}
		if line.start.x < domain[0] {
			domain[0] = line.start.x
		}
		if line.end.x > domain[1] {
			domain[1] = line.end.x
		}
		if line.start.y < rng[0] {
			rng[0] = line.start.y
		}
		if line.end.y > rng[1] {
			rng[1] = line.end.y
		}

	}
	// fmt.Println(domain)
	// fmt.Println(rng)
	// fmt.Println(len(lines))
	grid := make([][]int64, rng[1]-rng[0]+1)
	for y := rng[0]; y <= rng[1]; y += 1 {
		grid[y] = make([]int64, domain[1]-domain[0]+1)
	}
	for _, line := range lines {
		if line.vertical {
			x := line.start.x
			for y := line.start.y; y <= line.end.y; y += 1 {
				grid[y][x] += 1
			}
		} else if line.horizontal {
			y := line.start.y
			for x := line.start.x; x <= line.end.x; x += 1 {
				grid[y][x] += 1
			}
		} else { //diagonal
			//fmt.Println(line)
			x := line.start.x
			y := line.start.y
			deltaX := int64(1)
			if line.start.x > line.end.x {
				deltaX = -1
			}
			for y <= line.end.y {
				grid[y][x] += 1
				y += 1
				x += deltaX
			}
		}
	}

	result := 0
	for y := rng[0]; y <= rng[1]; y += 1 {
		//var line string
		for x := domain[0]; x <= domain[1]; x += 1 {
			count := grid[y][x]
			if count > 0 {
				//line += fmt.Sprint(count)
			} else {
				//line += fmt.Sprint(".")
			}
			if count > 1 {
				result += 1
			}
		}
		//fmt.Println(line)
	}

	return fmt.Sprint(result), nil
}

type Point struct {
	x int64
	y int64
}
type Line struct {
	start      Point
	end        Point
	vertical   bool
	horizontal bool
	flipped    bool
}

func ComparePoints(a, b Point) int {
	if a.y < b.y {
		return -1
	}
	if b.y < a.y {
		return 1
	}
	if a.x < b.x {
		return -1
	}
	if b.x < a.x {
		return 1
	}
	return 0
}

func ParsePoint(data string) (Point, error) {
	coords := strings.Split(data, ",")

	if len(coords) != 2 {
		return Point{0, 0}, fmt.Errorf("Unable to parse point: %s", data)
	}
	x, err := strconv.ParseInt(coords[0], 10, 64)
	if err != nil {
		return Point{0, 0}, err
	}
	y, err := strconv.ParseInt(coords[1], 10, 64)
	if err != nil {
		return Point{0, 0}, err
	}
	return Point{x, y}, nil
}
func ParseLine(data string) (Line, error) {
	split := strings.Split(data, " -> ")
	if len(split) != 2 {
		return Line{}, fmt.Errorf("Unable to parse line %s", data)
	}
	a, err := ParsePoint(split[0])
	if err != nil {
		return Line{}, err
	}
	b, err := ParsePoint(split[1])
	if err != nil {
		return Line{}, err
	}

	flipped := false
	if ComparePoints(a, b) > 0 {
		tmp := b
		b = a
		a = tmp
		flipped = true
	}
	return Line{a, b, isVertical(a, b), isHorizontal(a, b), flipped}, nil

}

func isVertical(a, b Point) bool {
	return a.x == b.x
}
func isHorizontal(a, b Point) bool {
	return a.y == b.y
}

func (l Line) Contains(p Point) bool {
	if l.vertical && l.start.x == p.x {
		return l.start.y <= p.y && l.end.y >= p.y
	}
	if l.horizontal && l.start.y == p.y {
		return l.start.x <= p.x && l.end.x >= p.x
	}
	return false
}
