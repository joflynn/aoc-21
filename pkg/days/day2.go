package days

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

type Position struct {
	x   int64
	y   int64
	aim int64
}

func (pos *Position) MoveNaive(direction string, distance int64) {
	switch direction {
	case "down":
		pos.y += distance
	case "up":
		pos.y -= distance
	case "forward":
		pos.x += distance
	}
}

func (pos *Position) Move(direction string, distance int64) {
	switch direction {
	case "down":
		pos.aim += distance
	case "up":
		pos.aim -= distance
	case "forward":
		pos.x += distance
		pos.y += distance * pos.aim
	}
}

func ParseMovement(command string) (string, int64, error) {
	tokens := strings.Split(command, " ")
	if len(tokens) != 2 {
		return "", 0, fmt.Errorf("Invalid command %s", command)
	}
	direction := tokens[0]
	distance, err := strconv.ParseInt(tokens[1], 10, 64)
	if err != nil {
		return "", 0, fmt.Errorf("Invalid command %s", command)
	}
	return direction, distance, nil
}

func Day2(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)
	pos := Position{x: 0, y: 0, aim: 0}
	for scanner.Scan() {
		command := scanner.Text()
		direction, distance, err := ParseMovement(command)
		if err != nil {
			return "", err
		}

		if part == PART_ONE {
			pos.MoveNaive(direction, distance)
		} else {
			pos.Move(direction, distance)
		}
	}
	value := fmt.Sprint(pos.x * pos.y)
	return value, nil
}
