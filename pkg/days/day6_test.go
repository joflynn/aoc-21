package days

import (
	"strings"
	"testing"
)

const Day6Input = `3,4,3,1,2`

const expectedDay6Part1 = "5934"

func TestDay6Part1(t *testing.T) {
	reader := strings.NewReader(Day6Input)
	value, err := Day6(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay6Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay6Part1, value)
	}
}

const expectedDay6Part2 = "26984457539"

func TestDay6Part2(t *testing.T) {
	reader := strings.NewReader(Day6Input)
	value, err := Day6(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay6Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay6Part2, value)
	}
}
