package days

import (
	"strings"
	"testing"
)

const Day10Input = `[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]`

const expectedDay10Part1 = "26397"

func TestDay10Part1(t *testing.T) {
	reader := strings.NewReader(Day10Input)
	value, err := Day10(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay10Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay10Part1, value)
	}
}

const expectedDay10Part2 = "288957"

func TestDay10Part2(t *testing.T) {
	reader := strings.NewReader(Day10Input)
	value, err := Day10(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay10Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay10Part2, value)
	}
}
