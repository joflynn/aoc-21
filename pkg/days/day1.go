package days

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
)

const INIT = -9999

func Day1(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)
	var previous int64 = INIT
	var count = 0

	len := 1
	if part == PART_TWO {
		len = 3
	}
	buf := make([]int64, len)
	idx := 0
	full := false

	for scanner.Scan() {
		current, err := strconv.ParseInt(scanner.Text(), 10, 64)
		if err != nil {
			return "", err
		}

		buf[idx] = current
		if idx+1 == len {
			full = true
		}
		idx = (idx + 1) % len

		next := sum(buf)
		if full {
			if previous != INIT {
				if increasing(previous, next) {
					count += 1
				}
			}
			previous = next
		}
	}
	return fmt.Sprint(count), nil
}

func sum(buf []int64) int64 {
	var total int64
	for _, cur := range buf {
		total += cur
	}
	return total
}
func increasing(a, b int64) bool {
	return a < b
}
