package days

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

func Day13(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)

	points := make([]Point, 0)
	pointMap := make(map[Point]bool)
	folds := make([]string, 0)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, "fold along x") || strings.Contains(line, "fold along y") {
			folds = append(folds, line)
		}
		if strings.Contains(line, ",") {
			point, err := ParsePoint(line)
			if err == nil {
				points = append(points, point)
				pointMap[point] = true
			}
		}
	}

	for _, fold := range folds {
		split := strings.Split(fold, "=")
		at, err := strconv.ParseInt(split[1], 10, 64)
		if err != nil {
			return "", err
		}
		if strings.Contains(split[0], "fold along x") {
			points = VerticalFold(points, pointMap, at)
		}
		if strings.Contains(split[0], "fold along y") {
			points = HorizontalFold(points, pointMap, at)
		}

		if part == PART_ONE {
			break
		}
		//fmt.Print(RenderMessage(points, pointMap))
	}

	if part == PART_TWO {
		message := RenderMessage(points, pointMap)
		return message, nil
	}
	//fmt.Printf("points %d\n", points)
	//fmt.Println(pointMap)
	return fmt.Sprint(len(points)), nil
}

func VerticalFold(points []Point, pointMap map[Point]bool, at int64) []Point {
	//fmt.Printf("vertical at %d\n", at)
	for _, point := range points {
		if point.x > at {
			pointMap[point] = false

			diff := point.x - at
			xconj := at - diff
			conj := Point{xconj, point.y}

			//fmt.Printf("flip %d -> %d\n", point, conj)
			if !pointMap[conj] {
				pointMap[conj] = true
			}

		}
	}
	newPoints := make([]Point, 0)
	for point, ok := range pointMap {
		if ok {
			newPoints = append(newPoints, point)
		}
	}
	return newPoints
}
func HorizontalFold(points []Point, pointMap map[Point]bool, at int64) []Point {
	//fmt.Printf("horizontal at %d\n", at)
	for _, point := range points {
		if point.y > at {
			pointMap[point] = false

			diff := point.y - at
			yconj := at - diff
			conj := Point{point.x, yconj}

			//fmt.Printf("flip %d -> %d\n", point, conj)
			if !pointMap[conj] {
				pointMap[conj] = true
			}

		}
	}
	newPoints := make([]Point, 0)
	for point, ok := range pointMap {
		if ok {
			newPoints = append(newPoints, point)
		}
	}
	return newPoints
}

func RenderMessage(points []Point, pointMap map[Point]bool) string {
	min := Point{999, 999}
	max := Point{0, 0}
	for _, point := range points {
		if point.x < min.x {
			min.x = point.x
		}
		if point.y < min.y {
			min.y = point.y
		}
		if point.x > max.x {
			max.x = point.x
		}
		if point.y > max.y {
			max.y = point.y
		}
	}

	lines := "\n"
	for j := min.y; j <= max.y; j += 1 {
		for i := min.x; i <= max.x; i += 1 {
			if pointMap[Point{i, j}] {
				lines += "#"
			} else {
				lines += "."
			}
		}
		lines += "\n"
	}

	return lines
}
