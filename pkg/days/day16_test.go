package days

import (
	"strings"
	"testing"
)

const Day16Input = ``

const expectedDay16Part1 = ""

func TestDay16Part1(t *testing.T) {
	reader := strings.NewReader(Day16Input)
	value, err := Day16(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay16Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay16Part1, value)
	}
}

const expectedDay16Part2 = ""

func TestDay16Part2(t *testing.T) {
	reader := strings.NewReader(Day16Input)
	value, err := Day16(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay16Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay16Part2, value)
	}
}
