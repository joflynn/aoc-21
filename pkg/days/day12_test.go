package days

import (
	"strings"
	"testing"
)

const Day12Input = `start-A
start-b
A-c
A-b
b-d
A-end
b-end`

const expectedDay12Part1 = "10"

func TestDay12Part1(t *testing.T) {
	reader := strings.NewReader(Day12Input)
	value, err := Day12(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay12Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay12Part1, value)
	}
}

const expectedDay12Part2 = "36"

func TestDay12Part2(t *testing.T) {
	reader := strings.NewReader(Day12Input)
	value, err := Day12(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay12Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay12Part2, value)
	}
}
