package days

import (
	"strings"
	"testing"
)

const Day14Input = `NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C`

const expectedDay14Part1 = "1588"

func TestDay14Part1(t *testing.T) {
	reader := strings.NewReader(Day14Input)
	value, err := Day14(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay14Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay14Part1, value)
	}
}

const expectedDay14Part2 = "2188189693529"

func TestDay14Part2(t *testing.T) {
	reader := strings.NewReader(Day14Input)
	value, err := Day14(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay14Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay14Part2, value)
	}
}
