package days

import (
	"strings"
	"testing"
)

const Day11Input = `5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526`

const expectedDay11Part1 = "1656"

func TestDay11Part1(t *testing.T) {
	reader := strings.NewReader(Day11Input)
	value, err := Day11(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay11Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay11Part1, value)
	}
}

const expectedDay11Part2 = "195"

func TestDay11Part2(t *testing.T) {
	reader := strings.NewReader(Day11Input)
	value, err := Day11(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay11Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay11Part2, value)
	}
}
