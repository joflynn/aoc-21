package days

import (
	"strings"
	"testing"
)

const input = `199
200
208
210
200
207
240
269
260
263`

const expectedDay1Part1 = "7"

func TestDay1Part1(t *testing.T) {
	reader := strings.NewReader(input)
	value, err := Day1(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error")
	}
	if value != expectedDay1Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay1Part1, value)
	}
}

const expectedDay1Part2 = "5"

func TestDay1Part2(t *testing.T) {
	reader := strings.NewReader(input)
	value, err := Day1(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error")
	}
	if value != expectedDay1Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay1Part2, value)
	}
}
