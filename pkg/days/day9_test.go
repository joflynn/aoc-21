package days

import (
	"strings"
	"testing"
)

const Day9Input = `2199943210
3987894921
9856789892
8767896789
9899965678`

const expectedDay9Part1 = "15"

func TestDay9Part1(t *testing.T) {
	reader := strings.NewReader(Day9Input)
	value, err := Day9(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay9Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay9Part1, value)
	}
}

const expectedDay9Part2 = "1134"

func TestDay9Part2(t *testing.T) {
	reader := strings.NewReader(Day9Input)
	value, err := Day9(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay9Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay9Part2, value)
	}
}
