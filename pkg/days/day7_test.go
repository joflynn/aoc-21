package days

import (
	"strings"
	"testing"
)

const Day7Input = `16,1,2,0,4,2,7,1,2,14`

const expectedDay7Part1 = "37"

func TestDay7Part1(t *testing.T) {
	reader := strings.NewReader(Day7Input)
	value, err := Day7(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay7Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay7Part1, value)
	}
}

const expectedDay7Part2 = "168"

func TestDay7Part2(t *testing.T) {
	reader := strings.NewReader(Day7Input)
	value, err := Day7(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay7Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay7Part2, value)
	}
}
