package days

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"strings"
)

type Rule struct {
	input string
	left  string
	right string
}

func Day14(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)

	scanner.Scan()
	template := scanner.Text()
	final := rune(template[len(template)-1])

	rules := make(map[string]Rule)
	current := make(map[string]int64)
	for scanner.Scan() {
		rule := scanner.Text()
		split := strings.Split(rule, " -> ")
		if len(split) != 2 {
			continue
		}
		left := string(split[0][0]) + split[1]
		right := split[1] + string(split[0][1])
		rules[split[0]] = Rule{split[0], left, right}
	}

	// fmt.Println(template)
	// fmt.Println(rules)

	iterations := 10
	if part == PART_TWO {
		iterations = 40
	}

	//Initialize
	for i := 0; i < (len(template) - 1); i += 1 {
		rule := template[i : i+2]
		if _, ok := rules[rule]; !ok {
			return "", fmt.Errorf("Rule not defined for %s", rule)
		}
		current[rule] += 1
	}
	//fmt.Println(next)

	for i := 0; i < iterations; i += 1 {
		next := make(map[string]int64)
		for k, v := range current {
			//build next hash
			rule := rules[k]

			next[rule.left] += v
			next[rule.right] += v
		}

		current = next
	}
	// fmt.Println(current)
	// return "", nil

	counts := make(map[rune]int64)
	for k, v := range current {
		first := rune(k[0])
		counts[first] += v
	}
	counts[final] += 1
	// for _, c := range sequence {
	// 	counts[c] += 1
	// }
	var min, max int64
	min = math.MaxInt64
	for _, count := range counts {
		if count < min {
			min = count
		}
		if count > max {
			max = count
		}
	}
	//fmt.Println(counts)
	return fmt.Sprint(max - min), nil
}
