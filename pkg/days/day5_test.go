package days

import (
	"strings"
	"testing"
)

const Day5Input = `0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2`

const expectedDay5Part1 = "5"

func TestDay5Part1(t *testing.T) {
	reader := strings.NewReader(Day5Input)
	value, err := Day5(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay5Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay5Part1, value)
	}
}

const expectedDay5Part2 = "12"

func TestDay5Part2(t *testing.T) {
	reader := strings.NewReader(Day5Input)
	value, err := Day5(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay5Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay5Part2, value)
	}
}
