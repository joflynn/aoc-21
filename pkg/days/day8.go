package days

import (
	"bufio"
	"fmt"
	"io"
	"sort"
	"strconv"
	"strings"
)

type SevenSegmentCipher struct {
	codes, message []string
}

func Day8(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)

	count := 0
	total := 0
	for scanner.Scan() {
		record, err := ParseSevenSegmentCipher(scanner.Text())
		if err != nil {
			return "", err
		}
		for _, digit := range record.message {
			if is1(digit) || is4(digit) || is7(digit) || is8(digit) {
				count += 1
			}
		}

		mapping := DecodeSevenSegmentCipher(record)
		//fmt.Println(mapping)
		decoded := 0
		for _, code := range record.message {
			digit, err := strconv.ParseInt(mapping[code], 10, 64)
			if err != nil {
				fmt.Println(code)
				return "", err
			}
			decoded *= 10
			decoded += int(digit)
		}
		//fmt.Println(decoded)
		total += decoded
	}
	if part == PART_ONE {
		return fmt.Sprint(count), nil
	}

	return fmt.Sprint(total), nil
}

func ParseSevenSegmentCipher(data string) (SevenSegmentCipher, error) {
	parts := strings.Split(data, " | ")
	if len(parts) != 2 {
		return SevenSegmentCipher{}, fmt.Errorf("Bad format, %s", data)
	}
	codes := strings.Split(parts[0], " ")
	message := strings.Split(parts[1], " ")

	if len(codes) > 10 {
		return SevenSegmentCipher{}, fmt.Errorf("Not enough codes")
	}
	if len(message) != 4 {
		return SevenSegmentCipher{}, fmt.Errorf("Wrong message length")
	}
	for i, code := range codes {
		codes[i] = sortChars(code)
	}
	for i, msg := range message {
		message[i] = sortChars(msg)
	}

	return SevenSegmentCipher{codes, message}, nil
}

func DecodeSevenSegmentCipher(cipher SevenSegmentCipher) map[string]string {
	var a, b, c, d, e, f, g string
	var cf, acf, bcdf, bd, eg string
	var candidates235, candidates069, candidates09, candidates35 []string
	ret := make(map[string]string)
	for _, code := range cipher.codes {
		if is1(code) {
			cf = code
			ret[code] = "1"
		}
		if is7(code) {
			acf = code
			ret[code] = "7"
		}
		if is4(code) {
			bcdf = code
			ret[code] = "4"
		}
		if is8(code) {
			ret[code] = "8"
		}
		if is2or3or5(code) {
			candidates235 = append(candidates235, code)
		}
		if is0or6or9(code) {
			candidates069 = append(candidates069, code)
		}
	}
	//use 4 to isolate segment a
	a = strings.Replace(strings.Replace(acf, string(cf[0]), "", 1), string(cf[1]), "", 1)
	a = removeSubstr(acf, cf)
	bd = removeSubstr(bcdf, cf)
	//use 6 to collapse segments c/f
	for _, candidate6 := range candidates069 {
		//6 is the one that is missing one of cf
		if !(strings.Contains(candidate6, string(cf[0])) && strings.Contains(candidate6, string(cf[1]))) {
			ret[candidate6] = "6"

			if strings.Contains(candidate6, string(cf[0])) {
				f = string(cf[0])
				c = string(cf[1])
			} else {
				c = string(cf[0])
				f = string(cf[1])
			}
		} else {
			candidates09 = append(candidates09, candidate6)
		}
	}
	//use 0 to collapse segments b/d
	for _, candidate0 := range candidates09 {
		//6 is the one that is missing one of bd
		if !(strings.Contains(candidate0, string(bd[0])) && strings.Contains(candidate0, string(bd[1]))) {
			ret[candidate0] = "0"
			if strings.Contains(candidate0, string(bd[0])) {
				b = string(bd[0])
				d = string(bd[1])
			} else {
				d = string(bd[0])
				b = string(bd[1])
			}
			eg = removeSubstr(candidate0, a+bcdf)

		} else {
			//nine solved
			ret[candidate0] = "9"
		}
	}

	//both e/g -> 2
	for _, candidate2 := range candidates235 {
		if strings.Contains(candidate2, string(eg[0])) && strings.Contains(candidate2, string(eg[1])) {
			ret[candidate2] = "2"
		} else {
			candidates35 = append(candidates35, candidate2)
		}
	}

	//use 2/3 to collapse e/g
	for _, candidate3 := range candidates35 {
		if strings.Contains(candidate3, c) && strings.Contains(candidate3, f) {
			ret[candidate3] = "3"
			if strings.Contains(candidate3, string(eg[0])) {
				g = string(eg[0])
				e = string(eg[1])
			} else {
				e = string(eg[0])
				g = string(eg[1])
			}

		} else {
			ret[candidate3] = "5"
		}
	}
	ret["a"] = a
	ret["b"] = b
	ret["c"] = c
	ret["d"] = d
	ret["e"] = e
	ret["f"] = f
	ret["g"] = g

	return ret
}

func is1(digit string) bool {
	return len(digit) == 2
}

func is7(digit string) bool {
	return len(digit) == 3
}

func is4(digit string) bool {
	return len(digit) == 4
}

func is8(digit string) bool {
	return len(digit) == 7
}
func is2or3or5(digit string) bool {
	return len(digit) == 5
}
func is0or6or9(digit string) bool {
	return len(digit) == 6
}
func removeSubstr(value, remove string) string {
	removed := value
	for i := 0; i < len(remove); i += 1 {
		char := string(remove[i])
		removed = strings.Replace(removed, char, "", 1)
	}
	return removed
}
func sortChars(unsorted string) string {
	s := strings.Split(unsorted, "")
	sort.Strings(s)
	return strings.Join(s, "")
}
