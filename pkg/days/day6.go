package days

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

const SIMULATION_LENGTH = 80
const SIMULATION_LENGTH_PART_TWO = 256
const SPAWN_LENGTH = 7

func Day6(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)
	scanner.Scan()
	school := ParseFish(scanner.Text())
	nursery := make(chan int64, 3)

	//Prime the channel for a 2 day lag
	nursery <- 0
	nursery <- 0

	length := SIMULATION_LENGTH
	if part == PART_TWO {
		length = SIMULATION_LENGTH_PART_TWO
	}

	for i := 0; i <= length; i += 1 {
		index := i % SPAWN_LENGTH

		//spawn babies
		spawn := school[index]
		nursery <- spawn
		//graduate infants
		graduates := <-nursery
		school[index] = spawn + graduates

		//fmt.Printf("After %d day(s), bin: %d, status: %d, spawn: %d, grads: %d, total: %d\n", i, index, school, spawn, graduates, sum(school))
	}

	result := sum(school)
	return fmt.Sprint(result), nil
}

func ParseFish(data string) []int64 {
	split := strings.Split(data, ",")
	list := make([]int64, SPAWN_LENGTH)
	for _, v := range split {
		i, err := strconv.ParseInt(v, 10, 64)
		if err == nil {
			list[i-1] = list[i-1] + 1
		}
	}
	return list
}
