package days

import (
	"io"
	"bufio"
	"fmt"
	"gitlab.com/joflynn/aoc-21/pkg/util"

)

func Day11(part string, input io.Reader) (string, error) {
	scanner := bufio.NewScanner(input)

	grid := ParseHeightmap(scanner)
	
	total := 0
	tickCount := 100
	if part == PART_TWO {
		tickCount = 99999999
	}
	for i := 0; i < tickCount; i += 1 {
		currentFlashes := UpdateGrid(grid)
		if currentFlashes == 100 && part == PART_TWO {
			return fmt.Sprint(i+1), nil
		}
		total += currentFlashes
		//grid.Print()
	}

	return fmt.Sprint(total), nil
}

func UpdateGrid(grid HeightMap) int {
	flashes := util.NewQueue()
	flashed := make(map[HeightPoint]bool)
	for i := 0; i < grid.length; i += 1 {
		for j := 0; j < grid.width; j += 1 {
			UpdatePoint(grid, i, j, flashes)
		}
	}
	
	flashCount := 0
	for !flashes.Empty() {
		point := flashes.Front().(HeightPoint)
		if flashed[point] != true {

			flashCount += 1
			flashed[point] = true

			neighbors := grid.NeighborPointsWithDiagonal(point)
			for _, neighbor := range neighbors {
				if flashed[neighbor] != true {
					UpdatePoint(grid, neighbor.y, neighbor.x, flashes)
				}
			}
		}
	}

	//reset
	for p := range flashed {
		grid.grid[p.y][p.x] = 0
	}

	return flashCount
}

func UpdatePoint(grid HeightMap, y, x int, flashes *util.Queue) {
	grid.grid[y][x] += 1
	if grid.grid[y][x] > 9 {
		point := HeightPoint{0, y, x}
		flashes.Push(point)
	}
}
func (m HeightMap) NeighborPointsWithDiagonal(p HeightPoint) []HeightPoint{
	ret := m.NeighborPoints(p)
	if p.y > 0 && p.x < m.width -1 {
		//north
		//east
		ret = append(ret, HeightPoint{m.grid[p.y-1][p.x+1], p.y - 1, p.x+1})
	}
	if p.y < m.length-1 && p.x < m.width-1 {
		//south
		//east
		ret = append(ret, HeightPoint{m.grid[p.y+1][p.x+1], p.y+1, p.x + 1})
	}
	if p.y < m.length-1 && p.x > 0 {
		//south
		//west
		ret = append(ret, HeightPoint{m.grid[p.y+1][p.x-1], p.y + 1, p.x-1})
	}
	if p.y > 0 && p.x > 0 {
		//north
		//west
		ret = append(ret, HeightPoint{m.grid[p.y-1][p.x-1], p.y-1, p.x - 1})
	}
	return ret
}

func (m HeightMap) Print() {
	for _, line := range m.grid {
		fmt.Println(line)
	}
	fmt.Println()
}