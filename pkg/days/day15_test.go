package days

import (
	"strings"
	"testing"
)

const Day15Input = `1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581`

const expectedDay15Part1 = "40"

func TestDay15Part1(t *testing.T) {
	reader := strings.NewReader(Day15Input)
	value, err := Day15(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay15Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay15Part1, value)
	}
}

const expectedDay15Part2 = "317" // i think the spec was wrong "315"

func TestDay15Part2(t *testing.T) {
	reader := strings.NewReader(Day15Input)
	value, err := Day15(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay15Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay15Part2, value)
	}
}
