package days

import (
	"strings"
	"testing"
)

const day_Input = ``

const expectedDay_Part1 = ""

func TestDay_Part1(t *testing.T) {
	reader := strings.NewReader(day_Input)
	value, err := Day_(PART_ONE, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay_Part1 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay_Part1, value)
	}
}

const expectedDay_Part2 = ""

func TestDay_Part2(t *testing.T) {
	reader := strings.NewReader(day_Input)
	value, err := Day_(PART_TWO, reader)
	if err != nil {
		t.Errorf("Should not return an error, %w", err)
	}
	if value != expectedDay_Part2 {
		t.Errorf("Expect: '%s', Got: '%s'", expectedDay_Part2, value)
	}
}
