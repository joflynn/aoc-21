package input

import (
	"fmt"
	"io"
	"os"
)

func Input(day, part string) (io.Reader, error) {
	filename := fmt.Sprintf("../../data/%s/input.txt", day)
	return os.Open(filename)
}
