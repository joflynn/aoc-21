package bingo

import (
	"bufio"
	"fmt"
	"strconv"
	"strings"
)

const BOARD_LENGTH = 5
const BOARD_SIZE = BOARD_LENGTH * BOARD_LENGTH

type Board struct {
	values []int64
	marked []bool
}
type IBoard interface {
	Mark(int64) bool
	Won() bool
	Value() int64
}

func NewBoard(input *bufio.Scanner) (Board, error) {
	values, err := parseLines(input)
	if err != nil {
		return Board{}, err
	}
	if len(values) != BOARD_SIZE {
		return Board{}, fmt.Errorf("Wrong size Board")
	}
	marked := make([]bool, BOARD_SIZE)
	b := Board{values, marked}
	return b, nil
}

func (b Board) Mark(value int64) bool {
	matched := false
	for i, v := range b.values {
		if v == value {
			b.marked[i] = true
			matched = true
		}
	}
	return matched
}
func (b Board) Won() bool {
	for i := 0; i < BOARD_LENGTH; i += 1 {
		idxs := row(i)
		if b.allMarked(idxs) {
			return true
		}
	}
	for j := 0; j < BOARD_LENGTH; j += 1 {
		idxs := col(j)
		if b.allMarked(idxs) {
			return true
		}
	}
	return false
}
func (b Board) allMarked(indexes []int) bool {
	for _, idx := range indexes {
		if !b.marked[idx] {
			return false
		}
	}
	return true
}
func (b Board) Value() (total int64) {
	for i, marked := range b.marked {
		if !marked {
			total += b.values[i]
		}
	}
	return
}

func parseLines(input *bufio.Scanner) ([]int64, error) {
	var values []int64
	for len(values) < 25 {
		input.Scan()
		line := input.Text()
		for _, val := range strings.Split(line, " ") {
			if val != "" {

				v, err := strconv.ParseInt(val, 10, 64)
				if err != nil {
					return values, err
				}
				values = append(values, v)
			}
		}
	}
	return values, nil
}

func row(n int) (ret []int) {
	for i := 0; i < BOARD_LENGTH; i += 1 {
		next := n*BOARD_LENGTH + i
		ret = append(ret, next)
	}
	return
}
func col(n int) (ret []int) {
	for i := 0; i < BOARD_LENGTH; i += 1 {
		next := i*BOARD_LENGTH + n
		ret = append(ret, next)
	}
	return
}
