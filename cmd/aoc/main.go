package main

import (
	"errors"
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/joflynn/aoc-21/pkg/input"
)

func main() {
	app := &cli.App{
		Name:  "aoc",
		Usage: "Advent of Code 2021",

		Commands: []*cli.Command{
			{
				Name:   "run",
				Usage:  "run DAY [PART]",
				Action: run,
			},
		},
	}

	err := app.Run(os.Args)
	fatal(err)
}

func run(c *cli.Context) error {
	args := c.Args()
	if args.Len() < 1 {
		return errors.New("Run missing required DAY")
	}
	day := args.Get(0)
	part := "1"
	if args.Len() == 2 {
		part = args.Get(1)
	}

	fmt.Printf("Running: Day %s, Part %s\n", day, part)

	handlers := handlers()
	if handlers[day] == nil {
		return fmt.Errorf("Handler for Day %s is not configured", day)
	}

	input, err := input.Input(day, part)
	if err != nil {
		return err
	}

	result, err := handlers[day](part, input)
	fmt.Printf("Result: %s\n", result)
	return err
}

func fatal(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}
