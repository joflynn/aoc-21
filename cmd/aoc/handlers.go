package main

import (
	"io"

	"gitlab.com/joflynn/aoc-21/pkg/days"
)

type Handler = func(part string, input io.Reader) (string, error)

func handlers() map[string]Handler {

	handlers := make(map[string]Handler, 25)
	handlers["1"] = days.Day1
	handlers["2"] = days.Day2
	handlers["3"] = days.Day3
	handlers["4"] = days.Day4
	handlers["5"] = days.Day5
	handlers["6"] = days.Day6
	handlers["7"] = days.Day7
	handlers["8"] = days.Day8
	handlers["9"] = days.Day9
	handlers["10"] = days.Day10
	handlers["11"] = days.Day11
	handlers["12"] = days.Day12
	handlers["13"] = days.Day13
	handlers["14"] = days.Day14
	handlers["15"] = days.Day15
	handlers["16"] = days.Day16
	// handlers["17"] = days.Day17
	// handlers["18"] = days.Day18
	// handlers["19"] = days.Day19
	// handlers["20"] = days.Day20
	// handlers["21"] = days.Day21
	// handlers["22"] = days.Day22
	// handlers["23"] = days.Day23
	// handlers["24"] = days.Day24
	// handlers["2%"] = days.Day25
	return handlers
}
