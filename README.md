# Advent of Code 2021

Written in [go][golang]

## run

```bash
$ cd cmd/aoc
$ go build .
$ go run DAY [PART]
```

## test

```bash
$ make test
```

Runs all test cases in the pkg/days directory.  Should execute 1 or two tests for each day.

[golang]: https://go.dev
