
PHONY: test

build: cmd/aoc/aoc
	cd cmd/aoc &&	go build . 

test:
	cd pkg/days && go test 
